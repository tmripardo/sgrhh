module.exports = function(app){
    app.get('/escolaridades', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "escolaridades");
        
        generalDAO.list(function(err, result){
            var tableName = "Escolaridades";
            res.format({
                html: function(){
                    res.render("pages/list-escolaridades", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}