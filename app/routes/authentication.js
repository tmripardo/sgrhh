module.exports = function(app, passport) {
    var auth = require('../infra/isLoggedIn');
    var User = require('../models/user');
    var jwt = require("jsonwebtoken");
    var config = require('../../config/database-config.json');
    var auth = require('../infra/isLoggedIn');
    // =====================================
    // HOME PAGE (with login links) ========
    // =====================================
    app.get('/', passport.authenticate('jwt', { successRedirect: '/dashboard', failureRedirect: '/login', session: false }));

    app.get('/login', function(req, res){
        res.render('pages/login');
    })

    // Register new users
    app.post('/register', function(req, res) {
        if(!req.body.email || !req.body.password) {
            res.json({ success: false, message: 'Please enter email and password.' });
        } else {

            /*var newUser = new User({
                email: req.body.email,
                password: req.body.password
            });*/
            User.findOne({email: req.body.email}, function(err, user) {
                if (err) throw err;

                // check to see if theres already a user with that email
                if (user) {
                    res.status(403).send({ success: false, message: 'That email address already exists.'});
                } else {
                    var newUser = new User({
                        email: req.body.email,
                        password: req.body.password
                    });

                    // Attempt to save the user
                    newUser.save(function(err) {
                        if (err) throw err;
                        else{
                            res.json({ success: true, message: 'Successfully created new user.' });
                        }
                    });
                } 
            });
        }
    });

    app.post('/login', function(req, res) {  
        User.findOne({
          email: req.body.email
        }, function(err, user) {
          if (err) throw err;
      
          if (!user) {
            res.send({ success: false, message: 'Authentication failed. User not found.' });
          } else {
            // Check if password matches
            user.comparePassword(req.body.password, function(err, isMatch) {
              if (isMatch && !err) {
                // Create token if the password matched and no error was thrown
               
                var token = jwt.sign(user.toObject(), config.secret, { expiresIn: '10h' });
                //console.log(res.header);
                //res.json({ success: true, token: 'bearer ' + token });
                res.redirect("/dashboard");
              } else {
                res.send({ success: false, message: 'Authentication failed. Passwords did not match.' });
              }
            });
          }
        });
    });

    app.get('/dashboard', passport.authenticate('jwt', { session: false }), function (req, res) {
        res.json(
            {
                message: 'It worked!',
                user: 'User id is: ' + req.user.email
            }
        );
    });

};
