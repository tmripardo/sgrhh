module.exports = function(app){
    app.get('/unidades', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "unidades");
        
        generalDAO.list(function(err, result){
            var tableName = "Unidades";
            res.format({
                html: function(){
                    res.render("pages/list-unidades", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}