module.exports = function(app){
    app.get('/profissoes', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "profissoes");
        
        generalDAO.list(function(err, result){
            var tableName = "Profissões";
            res.format({
                html: function(){
                    res.render("pages/list-profissoes", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}