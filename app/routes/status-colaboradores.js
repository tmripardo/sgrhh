module.exports = function(app){
    app.get('/status-colaboradores', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "status_funcionarios");
        
        generalDAO.list(function(err, result){
            var tableName = "Status Colaboradores";
            res.format({
                html: function(){
                    res.render("pages/list-status-colaboradores", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}