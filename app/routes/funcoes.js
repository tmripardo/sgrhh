module.exports = function(app){
    app.get('/funcoes', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "funcoes");
        
        generalDAO.list(function(err, result){
            var tableName = "Funções";
            res.format({
                html: function(){
                    res.render("pages/list-funcoes", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}