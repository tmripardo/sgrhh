module.exports = function(app){
    app.get('/areas', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "areas");
        
        generalDAO.list(function(err, result){
            var tableName = "Areas";
            res.format({
                html: function(){
                    res.render("pages/list-areas", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}
