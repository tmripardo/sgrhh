module.exports = function(app){
    app.get('/cargos', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "cargos");
        
        generalDAO.list(function(err, result){
            var tableName = "Cargos";
            res.format({
                html: function(){
                    res.render("pages/list-cargos", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}