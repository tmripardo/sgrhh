module.exports = function(app){
    app.get('/conselhos', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "conselhoscp");
        
        generalDAO.list(function(err, result){
            var tableName = "Conselhos";
            res.format({
                html: function(){
                    res.render("pages/list-conselhos", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}