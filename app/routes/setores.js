module.exports = function(app){
    app.get('/setores', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "setores");
        
        generalDAO.list(function(err, result){
            var tableName = "Setores";
            res.format({
                html: function(){
                    res.render("pages/list-setores", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}