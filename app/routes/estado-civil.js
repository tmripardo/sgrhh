module.exports = function(app){
    app.get('/estado-civil', function(req, res){
        //console.log("listando");
        var connection = app.infra.connectionFactoryMySQL();
        var generalDAO = new app.infra.GeneralDAO(connection, "estado_civil");
        
        generalDAO.list(function(err, result){
            var tableName = "Estado Civil";
            res.format({
                html: function(){
                    res.render("pages/list-estado-civil", {list:result, tableName}); 
                },
                json: function(){
                    res.json(result);
                }
            });
        });
        connection.end();
    });
}