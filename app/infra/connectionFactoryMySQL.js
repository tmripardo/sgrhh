var mysql = require('mysql');

function createDBConnection() {
    var con = mysql.createConnection({
        host : 'localhost',
        user : 'root',
        password : 'k95e15',
        database : 'sgrhdb'
    });
    
    con.connect(function(err){
        if(err){
            console.log('Error connecting to Db');
        } else{
            console.log('Connection established with Database');
            
        }
        
    });
    return con;
} 

// wrapper
module.exports = function(){
    return createDBConnection;
}