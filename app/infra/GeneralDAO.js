function GeneralDAO(connection, tableName){
    this._connection = connection;
    this._tableName = tableName;
}

GeneralDAO.prototype.list = function(callback){
    this._connection.query('select * from '+this._tableName, callback);
}

GeneralDAO.prototype.save = function(contato, callback){
    this._connection.query('insert into '+this._tableName+' set ?', contato, callback);
}

GeneralDAO.prototype.update = function(contato, callback){
    console.log(contato);
    this._connection.query('update '+this._tableName+' set ? where id = '+contato.id, contato, callback);
}

GeneralDAO.prototype.delete = function(contato, callback){
    this._connection.query('delete from '+this._tableName+' where name = ?', contato.name, callback);
}

/*GeneralDAO.prototype.listByName = function(contato, callback){
    if(contato.name != ''){
        this._connection.query('select * from contatos where name = ?', contato.name, callback);
    } 
    else if(contato.nickname != ''){
        this._connection.query('select * from contatos where nickname = ?', contato.nickname, callback);
    }
    else{
        this._connection.query('select * from contatos', callback);
    }
}*/

module.exports = function(){
    return GeneralDAO;
}
