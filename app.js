var app = require('./config/express')();
var port = process.env.PORT || 8080;

app.listen(port);
console.log(`Server is running on port ${port}.`);