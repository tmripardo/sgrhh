var express = require('express');
var load = require('express-load');
var app = express();
var bodyParser = require('body-parser');
var passport = require('passport');
var morgan = require('morgan');

module.exports = function() {

    //var app = express();
    app.set('view engine', 'ejs');
    app.set('views','./public/views');

    app.use('/styles', express.static(process.cwd() + '/public/styles'));
    app.use('/img', express.static(process.cwd() + '/public/img'));
    app.use('/vendor', express.static(process.cwd() + '/public/vendor'));
    app.use('/css', express.static(process.cwd() + '/public/css'));
    app.use('/js', express.static(process.cwd() + '/public/js'));
    app.use('/dist', express.static(process.cwd() + '/public/dist'));

    // log every request to the console
    app.use(morgan('dev'));
    app.use(bodyParser.urlencoded({extended: true}));
    app.use(bodyParser.json());
    app.use(function(req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
        next();
    });

    //middlewareParser
    // required for passport
    require('../config/passport')(passport); 
    app.use(passport.initialize());

    // load infrastructure
    load('infra', {cwd: 'app'}).into(app);

    // load routes
    require('../app/routes/authentication.js')(app, passport);
   
    // Tables
    require('../app/routes/areas.js')(app);
    require('../app/routes/cargos.js')(app);
    require('../app/routes/funcoes.js')(app);
    require('../app/routes/profissoes.js')(app);
    require('../app/routes/setores.js')(app);
    require('../app/routes/unidades.js')(app);
    require('../app/routes/conselhos.js')(app);
    require('../app/routes/escolaridades.js')(app);
    require('../app/routes/status-colaboradores.js')(app);
    require('../app/routes/estado-civil.js')(app);

    // Meta, users and configurations
    require('../app/routes/sobre.js')(app);

    /*load('routes',{cwd: 'app'})
        .then('infra')
        .into(app);*/

    return app;

}